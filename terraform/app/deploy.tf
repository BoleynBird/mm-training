resource "aws_s3_bucket" "codepipeline-artifacts" {
    bucket = "${var.application}-${var.environment}-pipeline-artifacts"
    acl    = "private"
}

resource "aws_s3_bucket" "codepipeline-source" {
    bucket = "${var.application}-${var.environment}-pipeline-source"
    acl    = "private"

    # Versioning must be enabled to use as a codepipeline source.
    versioning {
        enabled = true
    }
}

# resource "aws_s3_bucket" "serverless-deployment" {
#     bucket = "${var.s3_serverless_deployment_bucket}"
#     acl    = "private"
# }

resource "aws_iam_role" "codepipeline-role" {
    name = "${var.application}-${var.environment}-deploy-codepipeline-role"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codepipeline.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codepipeline-policy" {
    name = "${var.application}-${var.environment}-deploy-codepipeline-policy"
    role = aws_iam_role.codepipeline-role.id
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect":"Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "${aws_s3_bucket.codepipeline-artifacts.arn}",
        "${aws_s3_bucket.codepipeline-artifacts.arn}/*",
        "${aws_s3_bucket.codepipeline-source.arn}",
        "${aws_s3_bucket.codepipeline-source.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "codebuild:BatchGetBuilds",
        "codebuild:StartBuild"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "codebuild-role" {
    name = "${var.application}-${var.environment}-deploy-codebuild-role"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codebuild-policy" {
    role = aws_iam_role.codebuild-role.name
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "events:*"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:DeleteLogGroup",
        "logs:DeleteLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "${aws_s3_bucket.app.arn}",
        "${aws_s3_bucket.app.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "${aws_s3_bucket.app-base-source.arn}",
        "${aws_s3_bucket.app-base-source.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "${aws_s3_bucket.codepipeline-artifacts.arn}",
        "${aws_s3_bucket.codepipeline-artifacts.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "${aws_s3_bucket.serverless-deployment-bucket.arn}",
        "${aws_s3_bucket.serverless-deployment-bucket.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:CreateBucket",
        "s3:ListAllMyBuckets",
        "s3:GetBucketLocation"
      ],
      "Resource": [
        "arn:aws:s3:::*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "secretsmanager:GetSecretValue",
        "secretsmanager:ListSecrets"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:*Vpc*",
        "ec2:*Subnet*",
        "ec2:*Gateway*",
        "ec2:*Vpn*",
        "ec2:*Route*",
        "ec2:*Address*",
        "ec2:*SecurityGroup*",
        "ec2:*Network*",
        "ec2:*DhcpOptions*",
        "ec2:Describe*",
        "ec2:createTags"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "cloudformation:*"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "apigateway:*"
      ],
      "Resource": [
        "arn:aws:apigateway:${var.aws_region}::/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "iam:GetRole",
        "iam:CreateRole",
        "iam:DeleteRole",
        "iam:PassRole",
        "iam:PutRolePolicy",
        "iam:DeleteRolePolicy",
        "iam:DetachRolePolicy",
        "iam:AttachRolePolicy"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
          "lambda:AddPermission",
          "lambda:InvokeFunction",
          "lambda:DeleteFunction",
          "lambda:PublishVersion",
          "lambda:List*",
          "lambda:CreateFunction",
          "lambda:Get*",
          "lambda:RemovePermission",
          "lambda:CreateAlias",
          "lambda:Update*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
POLICY
}

resource "aws_codebuild_project" "codebuild" {
    name = "${var.application}-${var.environment}-deploy"
    description  = "Execute admin deployment script"
    build_timeout = "5"
    service_role = aws_iam_role.codebuild-role.arn

    artifacts {
        type = "CODEPIPELINE"
    }

    # We run migrations, so we need to have access to the vpc
    # private subnets where rds is located.

    # vpc_config {
    #     vpc_id = "${aws_vpc.default.id}"
    #     subnets = "${aws_subnet.private_subnets.*.id}"
    #     security_group_ids = [
    #         "${aws_security_group.rds.id}"
    #     ]
    # }

    environment {
        compute_type    = "BUILD_GENERAL1_SMALL"
        image           = "aws/codebuild/nodejs:10.14.1"
        type            = "LINUX_CONTAINER"
        privileged_mode = true

        environment_variable {
            name  = "APPLICATION"
            value = var.application
        }

        environment_variable {
            name  = "ENVIRONMENT"
            value = var.environment
        }

        environment_variable {
            name = "APP_BUCKET"
            value = var.s3_app_bucket
        }

        environment_variable {
            name  = "SERVERLESS_SERVICE_NAME"
            value = var.serverless_service_name
        }

        environment_variable {
            name  = "SERVERLESS_DEPLOYMENT_BUCKET"
            value = var.serverless_deployment_bucket
        }

        environment_variable {
            name  = "SERVERLESS_REGION"
            value = var.aws_region
        }
    }

    source {
        type = "CODEPIPELINE"
        buildspec = <<BUILDSPEC
version: 0.2
phases:
  install:
    commands:
      - sudo apt-get update
      - sudo apt-get install jq
      - npm install -g serverless
  pre_build:
    commands:
      - echo "Setting environment variables"
      - export SOURCE_BUCKET=$(cat input.json | jq -r .bucket)
      - export SOURCE_ARTEFACT=$(cat input.json | jq -r .artefact)
      - echo "SOURCE_BUCKET=$SOURCE_BUCKET"
      - echo "SOURCE_ARTEFACT=$SOURCE_ARTEFACT"
  build:
    commands:
      - echo "Getting release"
      - aws s3 cp s3://$SOURCE_BUCKET/$SOURCE_ARTEFACT release.tar.gz
      - echo "Extracting release"
      - tar -xvf release.tar.gz
      - echo "Executing deploy script"
      - chmod +x ./bin/deploy && ./bin/deploy
BUILDSPEC
    }

    tags = {
        application = var.application
        environment = var.environment
    }
}

resource "aws_codepipeline" "codepipeline" {

    name = "${var.application}-${var.environment}-deploy"
    role_arn = aws_iam_role.codepipeline-role.arn

    artifact_store {
        location = aws_s3_bucket.codepipeline-artifacts.bucket
        type = "S3"
    }

    stage {
        name = "Source"

        action {
            name             = "Source"
            category         = "Source"
            owner            = "AWS"
            provider         = "S3"
            version          = "1"
            output_artifacts = [ "SourceOutput" ]

            configuration = {
                S3Bucket = aws_s3_bucket.codepipeline-source.bucket
                S3ObjectKey = "input.zip"
            }
        }
    }

    stage {
        name = "Build"

        action {
            name            = "Build"
            category        = "Build"
            owner           = "AWS"
            provider        = "CodeBuild"
            input_artifacts = [ "SourceOutput" ]
            version         = "1"

            configuration = {
                ProjectName = "${var.application}-${var.environment}-deploy"
            }
        }
    }
}
