# Specify the provider
provider "aws" {
    profile = var.aws_profile
    region = var.aws_region
}

# use s3 to store terraform state and locks
# It does not have to be in the same region as the app.
# We also push out the bucket name in to var files with the filename
# based from the app/env since we cannot interpolate and construct the name here.
terraform {
    backend "s3" {
        encrypt = true
        key = "training.tfstate"
    }
}

variable "application" {}
variable "environment" {}

variable "aws_profile" {}
variable "aws_region" {}

variable "base_source_bucket" {}

variable "s3_app_bucket" {}
variable "cloudfront_alias" {}
variable "cloudfront_certificate_arn" {}
variable "serverless_deployment_bucket" {}
variable "serverless_service_name" {}
variable "api_domain_name" {}
variable "api_origin_path" {}
