resource "aws_s3_bucket" "app" {
    bucket = var.s3_app_bucket
    acl    = "public-read"

    website {
        index_document = "index.html"
        error_document = "index.html"
    }
}

resource "aws_cloudfront_origin_access_identity" "app" {
    comment = "${var.application} ${var.environment} app s3 origin access policy"
}

data "aws_iam_policy_document" "app_policy_doc" {
    statement {
        actions   = ["s3:GetObject"]
        resources = ["${aws_s3_bucket.app.arn}/*"]

        principals {
            type        = "AWS"
            identifiers = [aws_cloudfront_origin_access_identity.app.iam_arn]
        }
    }

    statement {
        actions   = ["s3:ListBucket"]
        resources = [aws_s3_bucket.app.arn]

        principals {
            type        = "AWS"
            identifiers = [aws_cloudfront_origin_access_identity.app.iam_arn]
        }
    }
}

resource "aws_s3_bucket_policy" "app_policy" {
    bucket = aws_s3_bucket.app.id
    policy = data.aws_iam_policy_document.app_policy_doc.json
}

resource "aws_cloudfront_distribution" "app" {
    depends_on  = [aws_s3_bucket.app]

    origin {
        domain_name = aws_s3_bucket.app.bucket_domain_name
        origin_id   = "s3-bucket-app"

        s3_origin_config {
            origin_access_identity = aws_cloudfront_origin_access_identity.app.cloudfront_access_identity_path
        }
    }

    origin {
        origin_id   = "api"
        origin_path = var.api_origin_path
        domain_name = var.api_domain_name

        custom_origin_config {
            http_port = 80
            https_port = 443
            origin_protocol_policy = "https-only"
            origin_ssl_protocols = ["TLSv1", "TLSv1.1", "TLSv1.2"]
        }
    }

    enabled = true
    price_class = "PriceClass_100"

    aliases = [var.cloudfront_alias]
    default_root_object = "index.html"

    default_cache_behavior {
        allowed_methods  = ["GET", "HEAD"]
        cached_methods   = ["GET", "HEAD"]
        target_origin_id = "s3-bucket-app"

        forwarded_values {
            query_string = false

            cookies {
                forward = "none"
            }
        }

        viewer_protocol_policy = "redirect-to-https"
        default_ttl = 0
    }

    ordered_cache_behavior {

        target_origin_id = "api"
        path_pattern = "/api/*"

        allowed_methods = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
        cached_methods = ["GET", "HEAD"]

        forwarded_values {
            query_string = true
            cookies {
                forward = "none"
            }
        }

        default_ttl = 0
        viewer_protocol_policy = "redirect-to-https"
    }

    custom_error_response {
        error_code = 404
        response_code = 200
        response_page_path = "/index.html"
        error_caching_min_ttl = 0
    }

    restrictions {
        geo_restriction {
            restriction_type = "none"
        }
    }

    viewer_certificate {
        cloudfront_default_certificate = false
        acm_certificate_arn = var.cloudfront_certificate_arn
        minimum_protocol_version = "TLSv1.1_2016"
        ssl_support_method = "sni-only"
    }

    tags = {
        Name        = "${var.application} ${var.environment} app cloudfront"
        aws_profile = var.aws_profile
        application = var.application
        environment = var.environment
        managedby   = "terraform"
    }
}
