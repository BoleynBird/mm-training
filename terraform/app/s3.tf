# This bucket is where bitbucket pipelinne will dump our project source
resource "aws_s3_bucket" "app-base-source" {
    bucket = var.base_source_bucket
    acl    = "private"
}

# This bucket is the serverless deployment bucket, as defined in serverless.yml
# ie. the serverless state
resource "aws_s3_bucket" "serverless-deployment-bucket" {
    bucket = var.serverless_deployment_bucket
    acl    = "private"
}
