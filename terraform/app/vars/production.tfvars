application = "training"
environment = "production"
aws_region = "eu-west-1"

# This is the published website bucket, behind cloudfront, the actual static website
s3_app_bucket = "training-app-hx48dy2"

cloudfront_alias = "www.training.michelle-morgan.co.uk"
cloudfront_certificate_arn = "arn:aws:acm:us-east-1:421125635330:certificate/4492c8bf-5b63-458c-92ef-66e45b439e31"

# This is where the base artefacts get pushed from bitbuket pipeline
base_source_bucket = "training-app-base-source2"

# This is the serverless bucket for serverless artefacts generated with serverless deploy
serverless_deployment_bucket = "training-serverless-deployment-bucket"

serverless_service_name = "training-hx48dy2"

# api gw domain name
api_domain_name = "b8ieibzdy3.execute-api.eu-west-1.amazonaws.com"
api_origin_path = "/production"