#!/bin/bash

set -e

BINDIR=$(/usr/bin/dirname $0)
APPDIR=$(bash -c "cd $BINDIR/..; pwd")

APPLICATION=$1
ENVIRONMENT=$2
BUILD_NUMBER=$3

if [ -z "$APPLICATION" ]; then
    echo "Missing application"
    exit 1
fi

if [ -z "$ENVIRONMENT" ]; then
    echo "Missing environment"
    exit 1
fi

if [ -z "$BUILD_NUMBER" ]; then
    echo "Missing version number"
    exit 1
fi

PIPELINE_SOURCE_BUCKET=${APPLICATION}-${ENVIRONMENT}-pipeline-source
APP_SOURCE_BASE_BUCKET=training-app-base-source2
ARTEFACT=release-$BUILD_NUMBER.tar.gz
AWS_PROFILE=${APPLICATION}-${ENVIRONMENT}

# This json file is used by the deploy codepipeline to trigger it and
# provide parameters specifying the releaseable artefact
cat << EOF > input.json
{
    "bucket": "$APP_SOURCE_BASE_BUCKET",
    "artefact": "$ARTEFACT"
}
EOF

zip input.zip input.json
aws s3 cp input.zip s3://$PIPELINE_SOURCE_BUCKET/input.zip --profile $AWS_PROFILE
rm input.zip input.json
